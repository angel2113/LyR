# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse, JsonResponse
from .forms import SeForm

from .models import Busqueda, Tweet
from .sinAPI import *
from .conAPI import *
import json
import csv

import json, re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
import numpy as np

class Home(View):
    def get(self, request, *args, **kwargs):
        form = SeForm(request.POST or None)
        context = {
            'form': form
        }
        return render(request, 'gris.html', context=context)# Create your views here.

def busqueda(request):
    if request.method == 'POST':
        texto = request.POST.get('busqueda')
        cantidad = request.POST.get('cantidad')
        tipo = request.POST.get('tipo')
        fecha_inicio = request.POST.get('fecha_inicio')
        fecha_fin = request.POST.get('fecha_fin')
        cantidad_elegida  = 5000 if int(cantidad) > 5000 else int(cantidad)
        # Parseo de las cadenas

        ## Guion bajo o buscar un nombre de usuario
        for b in texto.split(' '):
            if b.startswith('@'):
                obj = guarda_busqueda(b, cantidad_elegida, tipo, fecha_inicio, fecha_fin)
                down_user(b, obj, cantidad_elegida)
            else:
                if(tipo == 'presente'):
                    down_api_tweets(b)
                if(tipo == 'pasado'):
                    obj = guarda_busqueda(b, cantidad_elegida, tipo, fecha_inicio, fecha_fin)
                    down_all_tweets(b, obj, cantidad_elegida)

        return JsonResponse({'texto': texto})

def guarda_busqueda(busqueda, cantidad, tipo, fecha_inicio=None, fecha_fin=None):
    obj = Busqueda(
        busqueda=busqueda,
        tipo=tipo,
        cantidad=cantidad
    )
    obj.save()
    return obj

def down_user(busqueda, obj, cantidad):
    r = get_by_username(busqueda, cantidad)
    for tweet in r:
        new_insert = Tweet(
            busqueda=obj,
            ide=tweet.id,
            permalink=tweet.permalink,
            username=tweet.username,
            text=tweet.text,
            date=tweet.date,
            formatted_date=tweet.formatted_date,
            retweets=tweet.retweets,
            favorites=tweet.favorites,
            mentions=tweet.mentions,
            hashtags=tweet.hashtags,
            geo=tweet.geo,
            urls=tweet.urls,
            author_id=tweet.author_id
        )
        new_insert.save()

def down_all_tweets(busqueda, obj, cantidad):
    r = get_by_query(busqueda, cantidad)
    for tweet in r:
        new_insert = Tweet(
            busqueda=obj,
            ide=tweet.id,
            permalink=tweet.permalink,
            username=tweet.username,
            text=tweet.text,
            date=tweet.date,
            formatted_date=tweet.formatted_date,
            retweets=tweet.retweets,
            favorites=tweet.favorites,
            mentions=tweet.mentions,
            hashtags=tweet.hashtags,
            geo=tweet.geo,
            urls=tweet.urls,
            author_id=tweet.author_id
        )
        new_insert.save()


from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

consumer_key="vIOF1brad1Y0nOP2NL4l3IoTG"
consumer_secret="mEsOrxI7hGsP9KD1pZZNXumWkVkWE10vYOzMeEuIGuILaBhT0a"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token="3179474136-J4icCODyJSHQ6NmnTryxMzOrUyVoEZc0kC5BdIy"
access_token_secret="cUdH23ULO5kbCfj4N8D5ovDGxSTVeidcbAy5I2TWjZ6z9"
def down_api_tweets(referencia):
    # Registrar la busqueda
    obj = guarda_busqueda(referencia, 1, 'api')

    # Buscar
    l = StdOutListener(obj)
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    stream = Stream(auth, l)
    stream.filter(track=[referencia])
    # Guardar

class StdOutListener(StreamListener):

    def __init__(self, busqueda=None):
        self.busqueda  = busqueda

    def on_data(self, data):
        objeto = json.loads(data)

        new_insert = Tweet(
            busqueda=self.busqueda,
            ide=objeto['id'],
            username=objeto['user']['name'],
            text=objeto['text'],
            date=objeto['created_at'],
            retweets=objeto['retweet_count'],
            favorites=objeto['favorited'],
            mentions=objeto['entities']['user_mentions'],
            hashtags=objeto['entities']['hashtags'],
            geo=objeto['geo'],
            author_id=objeto['user']['id']
        )
        new_insert.save()
        print(objeto)
        return True

    def on_error(self, status):
        print(status)
        return False

#### despliegue de datos

from django.core import serializers
from datetime import datetime

def parseMyData(dct):
    if 'Date' in dct:
        timestamp = int(dct['Date'][6:-2])
        dct['Date'] = datetime.fromtimestamp(timestamp)
    return dct

# Obtiene las busquedas
def get_busquedas(request):
    objects = Busqueda.objects.all().values('folio', 'busqueda', 'cantidad')
    #return JsonResponse(list(objects), safe=False)
    for obj in objects:
        b = Busqueda(folio=obj['folio'])
        t = Tweet.objects.all().filter(busqueda=b).values()
        if len(t) == 0:
            Busqueda(folio=obj['folio']).delete()
        else:
            # Actualiza
            Busqueda.objects.filter(folio=obj['folio']).update(cantidad=len(t))

    return HttpResponse(json.dumps({'resultado':list(objects)}))

# descarga como json
def get_json(request):
    folio = request.POST.get('folio')
    b = Busqueda(folio=folio)

    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename="salida.json"'
    #dictionaries = [obj.as_dict() for obj in Tweet.objects.all().filter(busqueda=b).values()]
    output = []
    for d in Tweet.objects.all().filter(busqueda=b).values():
        #response.write(d)
        output.append(d)
    response.write(json.dumps(output))
    return response

# descarga como csv
def get_csv(request):
    folio = request.POST.get('folio')
    b  = Busqueda(folio=folio)

    # Escribe la cabecera del csv
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tweets.csv"'
    response['Content-Type'] = 'application/x-download'

    writer = csv.writer(response)
    writer.writerow(['ide', 'permalink', 'text', 'hashtags', 'geo', 'urls', 'author_id'])


    for e in Tweet.objects.all().filter(busqueda=b).values():
        try:
            writer.writerow([e['ide'], e['permalink'], e['username'], e['text'], e['hashtags'], e['geo'], e['urls'], e['author_id']])
        except:
            pass

    return response

def clean(query):
    stop = ["0","1","2","3","4","5","6","7","8","9","_","a","actualmente","acuerdo","adelante","ademas","además","adrede","afirmó","agregó","ahi","ahora","ahí","al","algo","alguna","algunas","alguno","algunos","algún","alli","allí","alrededor","ambos","ampleamos","antano","antaño","ante","anterior","antes","apenas","aproximadamente","aquel","aquella","aquellas","aquello","aquellos","aqui","aquél","aquélla","aquéllas","aquéllos","aquí","arriba","arribaabajo","aseguró","asi","así","atras","aun","aunque","ayer","añadió","aún","b","bajo","bastante","bien","breve","buen","buena","buenas","bueno","buenos","c","cada","casi","cerca","cierta","ciertas","cierto","ciertos","cinco","claro","comentó","como","con","conmigo","conocer","conseguimos","conseguir","considera","consideró","consigo","consigue","consiguen","consigues","contigo","contra","cosas","creo","cual","cuales","cualquier","cuando","cuanta","cuantas","cuanto","cuantos","cuatro","cuenta","cuál","cuáles","cuándo","cuánta","cuántas","cuánto","cuántos","cómo","d","da","dado","dan","dar","de","debajo","debe","deben","debido","decir","dejó","del","delante","demasiado","demás","dentro","deprisa","desde","despacio","despues","después","detras","detrás","dia","dias","dice","dicen","dicho","dieron","diferente","diferentes","dijeron","dijo","dio","donde","dos","durante","día","días","dónde","e","ejemplo","el","ella","ellas","ello","ellos","embargo","empleais","emplean","emplear","empleas","empleo","en","encima","encuentra","enfrente","enseguida","entonces","entre","era","erais","eramos","eran","eras","eres","es","esa","esas","ese","eso","esos","esta","estaba","estabais","estaban","estabas","estad","estada","estadas","estado","estados","estais","estamos","estan","estando","estar","estaremos","estará","estarán","estarás","estaré","estaréis","estaría","estaríais","estaríamos","estarían","estarías","estas","este","estemos","esto","estos","estoy","estuve","estuviera","estuvierais","estuvieran","estuvieras","estuvieron","estuviese","estuvieseis","estuviesen","estuvieses","estuvimos","estuviste","estuvisteis","estuviéramos","estuviésemos","estuvo","está","estábamos","estáis","están","estás","esté","estéis","estén","estés","ex","excepto","existe","existen","explicó","expresó","f","fin","final","fue","fuera","fuerais","fueran","fueras","fueron","fuese","fueseis","fuesen","fueses","fui","fuimos","fuiste","fuisteis","fuéramos","fuésemos","g","general","gran","grandes","gueno","h","ha","haber","habia","habida","habidas","habido","habidos","habiendo","habla","hablan","habremos","habrá","habrán","habrás","habré","habréis","habría","habríais","habríamos","habrían","habrías","habéis","había","habíais","habíamos","habían","habías","hace","haceis","hacemos","hacen","hacer","hacerlo","haces","hacia","haciendo","hago","han","has","hasta","hay","haya","hayamos","hayan","hayas","hayáis","he","hecho","hemos","hicieron","hizo","horas","hoy","hube","hubiera","hubierais","hubieran","hubieras","hubieron","hubiese","hubieseis","hubiesen","hubieses","hubimos","hubiste","hubisteis","hubiéramos","hubiésemos","hubo","i","igual","incluso","indicó","informo","informó","intenta","intentais","intentamos","intentan","intentar","intentas","intento","ir","j","junto","k","l","la","lado","largo","las","le","lejos","les","llegó","lleva","llevar","lo","los","luego","lugar","m","mal","manera","manifestó","mas","mayor","me","mediante","medio","mejor","mencionó","menos","menudo","mi","mia","mias","mientras","mio","mios","mis","misma","mismas","mismo","mismos","modo","momento","mucha","muchas","mucho","muchos","muy","más","mí","mía","mías","mío","míos","n","nada","nadie","ni","ninguna","ningunas","ninguno","ningunos","ningún","no","nos","nosotras","nosotros","nuestra","nuestras","nuestro","nuestros","nueva","nuevas","nuevo","nuevos","nunca","o","ocho","os","otra","otras","otro","otros","p","pais","para","parece","parte","partir","pasada","pasado","paìs","peor","pero","pesar","poca","pocas","poco","pocos","podeis","podemos","poder","podria","podriais","podriamos","podrian","podrias","podrá","podrán","podría","podrían","poner","por","por qué","porque","posible","primer","primera","primero","primeros","principalmente","pronto","propia","propias","propio","propios","proximo","próximo","próximos","pudo","pueda","puede","pueden","puedo","pues","q","qeu","que","quedó","queremos","quien","quienes","quiere","quiza","quizas","quizá","quizás","quién","quiénes","qué","r","raras","realizado","realizar","realizó","repente","respecto","s","sabe","sabeis","sabemos","saben","saber","sabes","sal","salvo","se","sea","seamos","sean","seas","segun","segunda","segundo","según","seis","ser","sera","seremos","será","serán","serás","seré","seréis","sería","seríais","seríamos","serían","serías","seáis","señaló","si","sido","siempre","siendo","siete","sigue","siguiente","sin","sino","sobre","sois","sola","solamente","solas","solo","solos","somos","son","soy","soyos","su","supuesto","sus","suya","suyas","suyo","suyos","sé","sí","sólo","t","tal","tambien","también","tampoco","tan","tanto","tarde","te","temprano","tendremos","tendrá","tendrán","tendrás","tendré","tendréis","tendría","tendríais","tendríamos","tendrían","tendrías","tened","teneis","tenemos","tener","tenga","tengamos","tengan","tengas","tengo","tengáis","tenida","tenidas","tenido","tenidos","teniendo","tenéis","tenía","teníais","teníamos","tenían","tenías","tercera","ti","tiempo","tiene","tienen","tienes","toda","todas","todavia","todavía","todo","todos","total","trabaja","trabajais","trabajamos","trabajan","trabajar","trabajas","trabajo","tras","trata","través","tres","tu","tus","tuve","tuviera","tuvierais","tuvieran","tuvieras","tuvieron","tuviese","tuvieseis","tuviesen","tuvieses","tuvimos","tuviste","tuvisteis","tuviéramos","tuviésemos","tuvo","tuya","tuyas","tuyo","tuyos","tú","u","ultimo","un","una","unas","uno","unos","usa","usais","usamos","usan","usar","usas","uso","usted","ustedes","v","va","vais","valor","vamos","van","varias","varios","vaya","veces","ver","verdad","verdadera","verdadero","vez","vosotras","vosotros","voy","vuestra","vuestras","vuestro","vuestros","w","x","y","ya","yo","z","él","éramos","ésa","ésas","ése","ésos","ésta","éstas","éste","éstos","última","últimas","último","últimos"]
    query = query.lower()

    # search others
    replace = {'<': '', '&': '', '*': '', '\\': '', '/': '|',
               'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u',
               'Á': 'a', 'É': 'e', 'Í': 'i', 'Ó': 'o', 'Ú': 'u',
               '      ': ' ', '\n': '', '\xc2\xa1': '', '\xc2\xbf': '',
               '\xc3\x81': 'a', '\xc3\x89': 'e', '\xc3\x8d': 'i', '\xc3\x93': 'o', '\xc3\x9a': 'u',
               '\xc3\x9c': 'u', '\xc3\xa1': 'a', '\xc3\xa9': 'e', '\xc3\xad': 'i', '\xc3\xb3': 'o', '\xc3\xba': 'u',
               '\xc3\xbc': 'u', '\xc3\xb1': 'n', '\xc3\x91': 'n'}
    regex = re.compile("(%s)" % "|".join(map(re.escape, replace.keys())))

    query = regex.sub(lambda x: str(replace[x.string[x.start():x.end()]]), query)
    # query = re.sub(r"[https|http]\S", "", query)
    aux = query.split(' ')
    new = [w for w in aux if w not in stop or w.startswith('http:') or w.startswith('https:') or w.startswith('www:')]
    return ' '.join(new)

# Descarga los kluster
def down_kmeans(request):
    iteraciones = 1000
    grupo = request.POST.get('grupos')
    folio = request.POST.get('folio')

    # Obtener los tweets
    b = Busqueda(folio=folio)
    docs = [clean(t['text']) for t in Tweet.objects.all().filter(busqueda=b).values()]

    ## Vectorizer word
    vectorizer = TfidfVectorizer(stop_words='english')
    X = vectorizer.fit_transform(docs)

    ## cluster
    number_clusters = int(grupo)
    model = KMeans(n_clusters=number_clusters, init='k-means++', max_iter=int(iteraciones), n_init=1)
    model.fit(X)
    print (len(model.labels_))

    # Escribe la cabecera del csv
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Klusters.csv"'
    response['Content-Type'] = 'application/x-download'

    writer = csv.writer(response)
    writer.writerow(['Tweet', 'Kluster'])

    for t, k  in zip(docs, model.labels_):
        try:
            writer.writerow([t.encode(), k])
        except:
            pass

    return response

# Genera los kluster devuelve los datos para las graficas
def kmeans(request):
    iteraciones = 1000
    grupo = request.POST.get('grupos')
    folio = request.POST.get('folio')

    # Obtener los tweets
    b = Busqueda(folio=folio)
    docs = [clean(t['text']) for t in Tweet.objects.all().filter(busqueda=b).values()]

    for t in Tweet.objects.all().filter(busqueda=b).values():
        #print(t['favorites'])
        if(int(t['retweets']) > 0):
            print(t['retweets'])
    ## Vectorizer word
    vectorizer = TfidfVectorizer(stop_words='english')
    X = vectorizer.fit_transform(docs)

    ## cluster
    number_clusters = int(grupo)
    model = KMeans(n_clusters=number_clusters, init='k-means++', max_iter=int(iteraciones), n_init=1)
    model.fit(X)

    # Salida para cluster
    order_centroids = model.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names()
    salida = list()
    aux = list()

    #print(model.labels_)
    #print(len(model.labels_))
    '''
    for i in range(number_clusters):
        if not i in aux:
            aux[i] = 1
        else:
            aux[i] += 1
            '''
    '''
    for i in range(number_clusters):
        if not i in salida:
            aux.append(i)
            aux[i] = list()
        for ind in order_centroids[i]:
            aux[i].append(terms[ind])
    '''
    for i in range(number_clusters):
        if not i in salida:
            salida.append(i)
            salida[i] = list()
        for d, m in zip(docs,model.labels_):
            if i == m:
                salida[i].append(d)



    tamanios = {}

    for s in model.labels_:
        if not str(s) in tamanios:
            tamanios[str(s)] = 1
        else:
            tamanios[str(s)] += 1
    print(tamanios)


    # Salida para las barras

    # Salida para la rueda
    salida_rueda = list()
    salida_rueda_u = list()
    salida_rueda_h = list()

    # cruzado
    names = list()
    imports = list()

    # hashtags
    names_h = list()
    imports_h = list()

    # Usuarios
    names_u = list()
    imports_u = list()


    for d in docs:
        entra = None
        entra_h = None
        entra_u = None
        for p in d.split(' '):
            # Cruzado
            if p.startswith('#') or p.startswith('@'):
                if entra is None:
                    if not p in names:
                        names.append(p)
                        imports.append(list())
                        entra = p
                else:
                    imports[names.index(entra)].append(p)

            # hashtags
            if p.startswith('#'):
                if entra_h is None:
                    if not p in names_h:
                        names_h.append(p)
                        imports_h.append(list())
                        entra_h = p
                else:
                    imports_h[names_h.index(entra_h)].append(p)

            # usuario
            if p.startswith('@'):
                if entra_u is None:
                    if not p in names_u:
                        names_u.append(p)
                        imports_u.append(list())
                        entra_u = p
                else:
                    imports_u[names_u.index(entra_u)].append(p)

    # Eliminar los vacios
    for p, a in zip(names, imports):
        if len(a) == 0:
            del(imports[names.index(p)])
            del(names[names.index(p)])

    # Eliminar los vacios
    for p, a in zip(names_u, imports_u):
        if len(a) == 0:
            del(imports_u[names_u.index(p)])
            del(names_u[names_u.index(p)])

    # Eliminar los vacios
    for p, a in zip(names_h, imports_h):
        if len(a) == 0:
            del(imports_h[names_h.index(p)])
            del(names_h[names_h.index(p)])


    # sacar los 50 más grandes
    output_u = zip(names_u, imports_u)
    output_u = sorted(output_u, key=lambda tup: tup[1])
    output_u = output_u[-200:]

    # sacar los 50 más grandes
    output = zip(names, imports)
    output = sorted(output, key=lambda tup: tup[1])
    output = output[-200:]

    # sacar los 50 más grandes
    output_h = zip(names_h, imports_h)
    output_h = sorted(output_h, key=lambda tup: tup[1])
    output_h = output_h[-200:]

    # Forma los diccionarios
    from random import randint
    for p,a in output:
        d = {"name":p, "size":(len(a)*1000), "imports":a}
        salida_rueda.append(d)

    # Forma los diccionarios
    for p,a in output_u:
        d = {"name":p, "size":(len(a)*1000), "imports":a}
        salida_rueda_u.append(d)

    # Forma los diccionarios
    for p,a in output_h:
        d = {"name":p, "size":(len(a)*1000), "imports":a}
        salida_rueda_h.append(d)


    ## Tamanio para las barras


    return HttpResponse(json.dumps({'datos':salida, 'circulo':salida_rueda, 'circulo_u':salida_rueda_u, 'circulo_h':salida_rueda_h, 'tamanios':tamanios}), content_type='application/json')
