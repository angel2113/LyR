function creaBurbujas(elementos){
  $('#graficaBurbujas').empty();
  var width = 960,
  height = 500,
  padding = 1.5, // separation between same-color nodes
  clusterPadding = 6, // separation between different-color nodes
  maxRadius = 12;

var n = 100, // total number of nodes
    m = elementos.length; // number of distinct clusters

var color = d3.scale.category10()
    .domain(d3.range(m));


// The largest node for each cluster.
var clusters = new Array(m);

var n_nodos = 0;
var aux_nodos = [];
for (var k = 0; k < elementos.length; k++) {
  for (var j = 0; j < elementos[k].length; j++) {

    test = elementos[k][j];
    var i = Math.floor(Math.random() * m),
        r = Math.sqrt((i + 1) / m * -Math.log(Math.random())) * maxRadius,
        d = {
            name: test,
            cluster: i,
            radius: r,
            x: Math.cos(i / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
            y: Math.sin(i / m * 2 * Math.PI) * 200 + height / 2 + Math.random()
        };
    if (!clusters[i] || (r > clusters[i].radius)) clusters[i] = d;
    aux_nodos.push(d);
  }
}
var nodes = aux_nodos;
//console.log(JSON.stringify(nodes));
var force = d3.layout.force()
    .nodes(nodes)
    .size([width, height])
    .gravity(.02)
    .charge(0)
    .on("tick", tick)
    .start();

var svg = d3.select("#graficaBurbujas").append("svg")
    .attr("width", width)
    .attr("height", height);

var node = svg.selectAll("circle")
    .data(nodes)
    .enter().append("g").call(force.drag);
//addcircle to the group
node.append("circle")
    .style("fill", function (d) {
    return color(d.cluster);
}).attr("r", function(d){return d.radius})
// add click
node.on("mouseover", function(d){
    // --> console.log("on click" + d.className);
    //alert(d['name']);
    $("#b_content").html(d['name']);
})

//add text to the group

node.append("text")
    .text(function (d) {
    return d.name;
})
.attr("dx", -10)
    .attr("dy", ".35em")
    .text(function (d) {
    //return d.name //<-- pone el texto
    return ''
})
    .style("stroke", "gray");

function tick(e) {
    node.each(cluster(10 * e.alpha * e.alpha))
        .each(collide(.5))
    //.attr("transform", functon(d) {});
    .attr("transform", function (d) {
        var k = "translate(" + d.x + "," + d.y + ")";
        return k;
    })

}

function click(e) {
    console.log('hola');
}


// Move d to be adjacent to the cluster node.
function cluster(alpha) {
    return function (d) {
        var cluster = clusters[d.cluster];
        if (cluster === d) return;
        var x = d.x - cluster.x,
            y = d.y - cluster.y,
            l = Math.sqrt(x * x + y * y),
            r = d.radius + cluster.radius;
        if (l != r) {
            l = (l - r) / l * alpha;
            d.x -= x *= l;
            d.y -= y *= l;
            cluster.x += x;
            cluster.y += y;
        }
    };
}

// Resolves collisions between d and all other circles.
function collide(alpha) {
    var quadtree = d3.geom.quadtree(nodes);
    return function (d) {
        var r = d.radius + maxRadius + Math.max(padding, clusterPadding),
            nx1 = d.x - r,
            nx2 = d.x + r,
            ny1 = d.y - r,
            ny2 = d.y + r;
        quadtree.visit(function (quad, x1, y1, x2, y2) {
            if (quad.point && (quad.point !== d)) {
                var x = d.x - quad.point.x,
                    y = d.y - quad.point.y,
                    l = Math.sqrt(x * x + y * y),
                    r = d.radius + quad.point.radius + (d.cluster === quad.point.cluster ? padding : clusterPadding);
                if (l < r) {
                    l = (l - r) / l * alpha;
                    d.x -= x *= l;
                    d.y -= y *= l;
                    quad.point.x += x;
                    quad.point.y += y;
                }
            }
            return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
        });
    };
}
}
