// Carga las busquedas realizadas
function load_busquedas() {
   $.ajax({
       type: 'POST',
       url: '/gris/busquedas/',
       data: {
           csrfmiddlewaretoken: '{{ csrf_token }}'
       },
       dataType: 'json',
       complete: function (data) {
           var result = JSON.parse(data.responseText);
           if(result != null){
               $('#busquedas').empty();
               for(i=0; i< result.resultado.length; i++){
                   var b = result.resultado[i];
                   var fila = document.createElement('tr');
                   var input = document.createElement('input');
                   input.type = 'radio';
                   input.value = b['folio'];
                   input.name = 'descarga';
                   var td0 = document.createElement('td');
                   td0.appendChild(input);
                   var td1 = document.createElement('td');
                   td1.innerHTML = b['busqueda'];
                   //var td2 = document.createElement('td');
                   //td2.innerHTML = b['fecha'];
                   var td3 = document.createElement('td');
                   td3.innerHTML = b['cantidad'];

                   var td4 = document.createElement('td');
                   td4.innerHTML = b['fecha'];

                   fila.append(td0);
                   fila.append(td1);
                   //fila.append(td2);
                   fila.append(td3);
                   //fila.append(td4);
                   $('#busquedas').append(fila);
               }
           }
       },
       error: function(){
           console.log('No trajo las busquedas');
       }
   });
}

// Al descargar csv
$('#descarga_csv').click(function () {
   var folio = $('input[name="descarga"]:checked').val();
   var url = '/gris/down_csv/';
   var form = $("<form action='" + url + "' method='post'>" +
             "<input type='hidden' name='folio' value='" + folio + "' />" +
             "{% csrf_token %}"+
             "</form>");
   $('body').append(form);
   form.submit();
   $("#descarga").modal('hide');
});

// Generar y descargar los kluster
$('#down_clusters').click(function(){
   var folio = $('input[name="descarga"]:checked').val();
   var tipo = $('input[name=tipo_grafica]:checked').val();
   var url = '/gris/down_kluster/';


   if(tipo === 'kmeans'){
       var grupos = $('#k_groups').val();
       // Hace la peticion de formulario

       var form = $("<form action='" + url + "' method='post'>" +
             "<input type='hidden' name='folio' value='" + folio + "' />" +
             "<input type='hidden' name='grupos' value='" + grupos + "' />" +
             "{% csrf_token %}"+
             "</form>");
       $('body').append(form);
       form.submit();
   }
});

// Al descargar json
$('#descarga_json').click(function () {
   var folio = $('input[name="descarga"]:checked').val();
   var url = '/gris/down_json/';
   var form = $("<form action='" + url + "' method='post'>" +
             "<input type='hidden' name='folio' value='" + folio + "' />" +
             "{% csrf_token %}"+
             "</form>");
   $('body').append(form);
   form.submit();
   $("#descarga").modal('hide');
});

// grafica
$('#gen_graphic').click(function () {
   console.log('click grafica');
   var folio = $('input[name="descarga"]:checked').val();
   var tipo = $('input[name=tipo_grafica]:checked').val();
   console.log(tipo);
   if(tipo === 'kmeans'){
       var grupos = $('#k_groups').val();
       get_kmeans(grupos, folio);
   }

});

function get_kmeans(grupos, folio){
   $.ajax({
       type: 'POST',
       url: '/gris/kmeans/',
       data: {
           csrfmiddlewaretoken: '{{ csrf_token }}',
           grupos: grupos,
           folio: folio
       },
       dataType: 'json',
       beforeSend: function(){
           $('#loader').show();
       },

       complete: function (resultadoJson) {
           var obj = JSON.parse(resultadoJson['responseText']);
           var datosJSON = obj['datos'];
           var circuloJSON = obj['circulo'];
           console.log(circuloJSON);
           //console.log(datosJSON);
           if(datosJSON!=null){
             creaBurbujas(datosJSON);
             creaBarras(datosJSON);
             creaCirculo(circuloJSON);
             $('#loader').hide();
           }
       },
       error: function(){
           console.log('No trajo las busquedas');
       }
   });
}
